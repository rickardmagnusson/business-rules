﻿using BUManager.Models;
using System;
using System.Collections.Generic;

namespace BUManager
{
    /// <summary>
    /// The rule engine
    /// </summary>
    public static partial class RuleProcessor
    {
        //Print status colored
        static Action<string> P = (s) => { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine(s); };
        static Action<string> F = (s) => { Console.ForegroundColor = ConsoleColor.Yellow; Console.WriteLine(s); };
        static Dictionary<RequestType, List<RegisterEvent>> Events = new Dictionary<RequestType, List<RegisterEvent>>();

        public static void Process(List<RegisterEvent> items)
        {
            items.ForEach(c=> ProcessItem(c));
            PrintDict();
        }


        private static void ProcessItem(RegisterEvent item)
        {
            // PreRun all Rules for the Event
            var validateIsBank = ValidateSource(item);
            var validateIsActive = ValidateStatus(item);
            var validateInsuranceType = IsInsuranceType(item);


            /* Now we can Group these Rules to meet a specific requirement.
             * Grouped Rule to match specific items
             * What we want to achieve gets more clear now.
             */
            var ShouldPassActiveAndIsBank =
                validateIsBank
                .And(validateIsActive);

             /*
             * The name "ShouldPassActiveAndIsBank" can later on, 
             * be used to run the event in a specific function 
             * or store the event in a table in a database. 
             * We now know what todo with this item.
             * 
             * The image(schema) describes what todo with an item that reaches this level.  
             */

            if (ShouldPassActiveAndIsBank.PassedItem){
                AddToDict(RequestType.IsActiveAndIsBank, item);
            }
            else {
                var ShouldBeInsuranceType = validateInsuranceType;

                if (ShouldBeInsuranceType.PassedItem)
                    AddToDict(RequestType.IsInsurance, item);
                else
                    AddToDict(RequestType.UnKnownType, item);
            }
        }

        #region Helpers

        private static void PrintDict()
        {
            foreach(var key in Events.Keys)
            {
                var list = Events[key];
                foreach (var evt in list)
                    if(key==RequestType.UnKnownType)
                        F($"Unknown type: {evt.Name}");
                    else
                        P($"Item found: {key} : {evt.Name}");
            }
        }

        private static void AddToDict(RequestType key, RegisterEvent item)
        {
            var items = new List<RegisterEvent>();

            if (!Events.ContainsKey(key)) {
                items.Add(item);
                Events.Add(key, items);
            }
            else
            {
                Events.TryGetValue(key, out items);
                items.Add(item);
            }
        }

        #endregion
    }
}