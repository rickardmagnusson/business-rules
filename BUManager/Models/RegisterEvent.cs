﻿
namespace BUManager.Models
{
    public class RegisterEvent
    {
        public string Name { get; set; }

        public string CustomerNumber { get; set; }

        public bool IsActive { get; set; }

        public string Source { get; set; }

        public RequestType Result { get; set; }

    }
}
