﻿

namespace BUManager
{
    /// <summary>
    /// An enum to set the final statement to achieve.
    /// </summary>
    public enum RequestType
    {
        IsActiveAndIsBank,
        IsInsurance,
        UnKnownType
    }
}
