﻿using BUManager.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BUManager.Rules
{
    public class ActivityStatusRule<T> : CompositeRule<T>
    {
        string target;

        public ActivityStatusRule(T source, string target) : base(source)
        {
            this.target = target;
        }

        public override bool IsSatisfiedBy(T o)
        {
            RegisterEvent item;
            if (typeof(T) == typeof(RegisterEvent)) 
                item = (o as RegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (item.IsActive)
                return true;
            return false;
        }
    }
}
