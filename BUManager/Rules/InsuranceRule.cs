﻿using BUManager.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BUManager.Rules
{
    /// <summary>
    /// Validates that the Source of this event is in range of the valid Source to be processed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InsuranceRule<T> : CompositeRule<T>
    {
        private string target;

        public InsuranceRule(T source, string target) : base(source)
        {
            this.target = target;
        }


        public override bool IsSatisfiedBy(T o)
        {
            RegisterEvent item;
            if (typeof(T) == typeof(RegisterEvent))
                item = (o as RegisterEvent);
            else
                throw new Exception("Type is not valid");

            return item.Source=="Insurance";
        }
    }
}
