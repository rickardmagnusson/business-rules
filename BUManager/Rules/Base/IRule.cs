﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BUManager.Rules
{
    /// <summary>
    /// Main interface for all rules
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRule<T>
    {
        bool PassedItem { get; }
        bool NotPassedItem { get; }

        bool IsSatisfiedBy(T o);

        CompositeRule<T> And(IRule<T> Rule);
        CompositeRule<T> Or(IRule<T> Rule);
        CompositeRule<T> Not(IRule<T> Rule);
    }
}
