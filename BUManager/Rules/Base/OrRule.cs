﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BUManager.Rules
{
    public class OrRule<T> : CompositeRule<T>
    {
        IRule<T> leftRule;
        IRule<T> rightRule;

        /// <summary>
        /// Add a Or statement to a group of rules
        /// </summary>
        /// <param name="source"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public OrRule(T source, IRule<T> left, IRule<T> right) : base(source)
        {
            this.leftRule = left;
            this.rightRule = right;
        }

        public override bool IsSatisfiedBy(T o)
        {
            return this.leftRule.IsSatisfiedBy(o)
                || this.rightRule.IsSatisfiedBy(o);
        }
    }
}
