﻿using BUManager.Models;
using BUManager.Rules;
using System;
using System.Collections.Generic;
using System.Text;

namespace BUManager
{
    /// <summary>
    /// This class contains (wrappers) helpers when combining rules.
    /// Wrapped by a Composite rule.
    /// </summary>
    public static partial class RuleProcessor
    {
        //Just to display that you can validate a list as well.
        private static List<string> ValidSourceList = new List<string> { "Bank" };

        //Bank, Insurance should fail as we only want a bank event to proceed.
        public static Func<RegisterEvent, CompositeRule<RegisterEvent>> ValidateSource = item => 
                 new SourceRule<RegisterEvent>(item, ValidSourceList);

        //(True or false) True if Active
        public static Func<RegisterEvent, CompositeRule<RegisterEvent>> ValidateStatus = item => 
                new ActivityStatusRule<RegisterEvent>(item, RuleHelper.ActivityStatus(item, true));

        //(True or false) True if Insurance
        public static Func<RegisterEvent, CompositeRule<RegisterEvent>> IsInsuranceType = item =>
                new InsuranceRule<RegisterEvent>(item, RuleHelper.InsuranceType(item, "Insurance"));
    }
}
