﻿using BUManager.Models;
using System;
using System.Collections.Generic;

namespace BUManager
{
    class Program
    {
        private static List<RegisterEvent> Items = new List<RegisterEvent>(){
            //As a result test, we can add the Result and see if it matches the produced result.
            new RegisterEvent{ Result = RequestType.IsActiveAndIsBank, Name="Rickard", CustomerNumber = "1", IsActive= true, Source="Bank"},  // Should pass all
            new RegisterEvent{ Result = RequestType.IsActiveAndIsBank, Name="Magnus", CustomerNumber = "5", IsActive= true, Source="Bank"},   // Should pass all
            new RegisterEvent{ Result = RequestType.IsInsurance, Name="Susana", CustomerNumber = "2", IsActive= true, Source="Insurance"},    // Should pass active but not by Source
            new RegisterEvent{ Result = RequestType.UnKnownType, Name="Anna", CustomerNumber = "3", IsActive= false, Source="Bank"},          // Should pass as Bank but not by Active
            new RegisterEvent{ Result = RequestType.UnKnownType, Name="Anders", CustomerNumber = "4", IsActive= false, Source="Private"}      // Should not pass any
        };

        static void Main(string[] args)
        {
            RuleProcessor.Process(Items);
            Console.ReadLine();
        }
    }
}
