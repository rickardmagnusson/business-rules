﻿using BUManager.Models;

namespace BUManager
{
    /// <summary>
    /// Class containing all functions required for Rule management
    /// </summary>
    public static partial class RuleHelper
    {
        /// <summary>
        /// This Rule will apply only to those who is Active
        /// </summary>
        /// <param name="item"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string ActivityStatus(this RegisterEvent item, bool status)
        {
            if (item.IsActive == status)
                return item.Name;
       
            return null;
        }


        /// <summary>
        /// This Rule will apply only to those who are of type Insurance
        /// </summary>
        /// <param name="item"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string InsuranceType(this RegisterEvent item, string target)
        {
            if (item.Source == target)
                return item.Name;

            return null;
        }
    }
}
